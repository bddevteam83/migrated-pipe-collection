import {Pipe, PipeTransform} from '@angular/core';

class Validation {
  static isFilterEnable(list: Array<any>, param: string, equality): boolean {
    if (list !== undefined && param !== undefined && equality !== undefined) {
      return (list.length && list.length !== 0);
    } else {
      return false;
    }
  }
}

/**
 * @deprecated
 */
@Pipe({
  name: 'cleanDataListByParams'
})
export class CleanDataListByParamsPipe implements PipeTransform {

  transform(list: Array<any>, param: string, equality: any): Array<any> {
    const container = [];
    if (Validation.isFilterEnable(list, param, equality)) {
      list.forEach(element => {
        if (element[param] !== null && element[param] !== equality) {
          container.push(element);
        }
      });
      return container;
    }
    return list;
  }

}
