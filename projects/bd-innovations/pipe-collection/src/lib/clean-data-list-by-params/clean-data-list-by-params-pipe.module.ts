import {NgModule} from '@angular/core';
import {CleanDataListByParamsPipe} from './clean-data-list-by-params.pipe';

@NgModule({
  declarations: [CleanDataListByParamsPipe],
  exports: [CleanDataListByParamsPipe]
})
export class CleanDataListByParamsPipeModule {
}
