import {IncludesPipe} from './includes.pipe';

describe('IncludesPipe', () => {

  const pipe = new IncludesPipe();

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should pass throw \'collection\' parameter, when it is falsy', function () {
    expect(pipe.transform(null, [])).toBeNull();
    expect(pipe.transform(undefined, [])).toBeUndefined();
  });

});
