import {Pipe, PipeTransform} from '@angular/core';

import * as _ from 'lodash';

/**
 * Checks if target is in collection using SameValueZero for equality comparisons. If fromIndex is negative,
 * it’s used as the offset from the end of collection.
 *
 * @param collection The collection to search.
 * @param target The value to search for.
 * @param fromIndex The index to search from.
 * @return boolean whether the target element is found or not; null if collection is falsy.
 */
@Pipe({
  name: 'includes'
})
export class IncludesPipe implements PipeTransform {

  transform(collection, target, fromIndex?): any {
    if (!collection) {
      return collection;
    }

    return _.includes(collection, target, fromIndex);
  }

}
