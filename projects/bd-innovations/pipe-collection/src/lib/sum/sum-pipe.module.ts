import {NgModule} from '@angular/core';
import {SumPipe} from './sum.pipe';

@NgModule({
  declarations: [SumPipe],
  exports: [SumPipe]
})
export class SumPipeModule {
}
