import {Pipe, PipeTransform} from '@angular/core';

/**
 * Return the sum of specified array of numbers
 *
 * @param array Array of numbers which sum is needed
 * @return number sum of array elements
 */
@Pipe({
  name: 'sum'
})
export class SumPipe implements PipeTransform {

  transform(array: number[]): number {
    return array.reduce((sum, value) => sum + value);
  }

}
