import {SumPipe} from './sum.pipe';

import * as faker from 'faker';

describe('SumPipe', () => {

  const pipe = new SumPipe();

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return sum of array elements', function () {
    const array: number[] = Array.apply(null, {length: faker.random.number()})
      .map(() => faker.random.number());
    const res: number = array.reduce((sum, value) => sum + value);

    expect(pipe.transform(array)).toBe(res);
  });

});
