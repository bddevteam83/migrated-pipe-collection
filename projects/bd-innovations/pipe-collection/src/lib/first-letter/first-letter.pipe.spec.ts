import {FirstLetterPipe} from './first-letter.pipe';

import * as faker from 'faker';

describe('FirstLetterPipe', () => {

  const pipe = new FirstLetterPipe();
  const value: string = faker.random.word();

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should pass throw \'value\' parameter, when it is falsy', function () {
    expect(pipe.transform(null)).toBeNull();
    expect(pipe.transform(undefined)).toBeUndefined();
    expect(pipe.transform('')).toBe('');
  });

  it('should return first letter of \'value\' parameter', function () {
    expect(pipe.transform(value)).toBe(value.charAt(0));
  });

});
