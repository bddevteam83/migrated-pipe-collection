import {Pipe, PipeTransform} from '@angular/core';

/**
 * Returns the first character of a string.
 *
 * @param value The string to work with.
 * @return string of first character of a string; null if value is falsy.
 */
@Pipe({
  name: 'firstLetter'
})
export class FirstLetterPipe implements PipeTransform {

  transform(value: string): string {
    if (!value) {
      return value;
    }

    return value.charAt(0);
  }

}
