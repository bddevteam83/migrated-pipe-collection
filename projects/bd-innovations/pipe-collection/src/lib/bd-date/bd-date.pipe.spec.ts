import {BdDatePipe} from './bd-date.pipe';
import * as faker from 'faker';
import {DatePipe} from '@angular/common';

describe('BdDatePipe', () => {

  const pipe = new BdDatePipe(new DatePipe('en-US'));
  const date = faker.date.past();

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should pass throw @value parameter, when it is falsy', function () {
    expect(pipe.transform(null)).toBeNull();
    expect(pipe.transform(undefined)).toBeUndefined();
  });

  it('should format data with default format when @format isn\'t specified', function () {
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();

    expect(pipe.transform(date)).toBe(`${day}/${month}/${year}`);
  });

  it('should format data with specified @format when it is passed', function () {
    const year = parseInt(date.getFullYear().toString().substr(-2), 10);
    const month = date.getMonth() + 1;
    const day = date.getDate();

    const format = 'M/d/yy';

    expect(pipe.transform(date, format)).toBe(`${month}/${day}/${year}`);
  });

  it('should support some formats from built in Angular DatePipe', function () {
    const months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec'
    ];

    const year = date.getFullYear();
    const month = months[date.getMonth()];
    const day = date.getDate();

    const format = 'mediumDate';


    expect(pipe.transform(date, format)).toBe(`${month} ${day}, ${year}`);
  });

});
