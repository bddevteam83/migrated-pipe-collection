import {Pipe, PipeTransform} from '@angular/core';
import {DatePipe} from '@angular/common';

/**
 *
 */
@Pipe({
  name: 'bdDate'
})
export class BdDatePipe implements PipeTransform {

  constructor(private date: DatePipe) {
  }

  transform(value: Date, format: string = 'd/M/yyyy'): any {
    if (!value) {
      return value;
    } else if (typeof value === 'string' && value === 'null') {
      // Andrey p****
      return null;
    }

    return this.date.transform(value, format);
  }

}
