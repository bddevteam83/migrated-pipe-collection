import {Pipe, PipeTransform} from '@angular/core';

import * as _ from 'lodash';

/**
 * Creates an array of elements corresponding to the given keys, or indexes, of collection
 * and converts all the elements of an array to string separated by the specified separator string or the default one.
 * Keys may be specified as individual arguments or as arrays of keys.
 *
 * @param object The object to iterate over.
 * @param props The property names or indexes of elements to pick, specified individually or in arrays.
 * @param separator A string used to separate one element of an array from the next in the resulting String. By default is ' '.
 * @return string of picked elements separated by the specified or default separator; Null if object is falsy.
 */
@Pipe({
  name: 'at'
})
export class AtPipe implements PipeTransform {

  transform(object, props, separator: string = ' '): string {
    if (!object) {
      return object;
    } else if (!props) {
      return props;
    } else if (object.length === 0 || props.length === 0) {
      return '';
    }

    return _.at(object, props).join(separator);
  }

}
