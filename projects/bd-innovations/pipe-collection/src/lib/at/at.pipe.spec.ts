import {AtPipe} from './at.pipe';

import * as faker from 'faker';

describe('AtPipe', () => {

  const pipe = new AtPipe();
  const object = {
    first: faker.random.word(),
    second: faker.random.word(),
    third: faker.random.word(),
    nested: {
      fourth: faker.random.word()
    }
  };

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should pass throw \'object\' parameter, when it is falsy', function () {
    const props = faker.random.word();

    expect(pipe.transform(null, props)).toBeNull();
    expect(pipe.transform(undefined, props)).toBeUndefined();
    expect(pipe.transform('', props)).toBe('');
  });

  it('should pass throw \'props\' parameter, when it is falsy', function () {
    expect(pipe.transform(object, null)).toBeNull();
    expect(pipe.transform(object, undefined)).toBeUndefined();
    expect(pipe.transform(object, '')).toBe('');
  });

  it('should return \' \', when \'object\' or \'props\' parameter is an empty array', function () {
    const props = faker.random.word();

    expect(pipe.transform([], props)).toEqual('');
    expect(pipe.transform(object, [])).toEqual('');
  });

  it('should return string of specified by \`props\` param property, when \'props\' is string', function () {
    const props = 'first';

    expect(pipe.transform(object, props)).toBe(object.first);
  });

  it('should return string of specified by \`props\` param nested property, when \'props\' is string with dot-format', function () {
    const props = 'nested.fourth';

    expect(pipe.transform(object, props)).toBe(object.nested.fourth);
  });

  it('should return string of specified by \`props\` param properties joined by \' \', when \'props\' is an array and custom \'separator\' isn\'t specified', function () {
    const props = ['first', 'second'];
    const separator = ' ';

    expect(pipe.transform(object, props)).toBe(`${object.first}${separator}${object.second}`);
  });

  it('should return string of specified by \`props\` param properties joined by specified \'separator\', when it is specified and \'props\' is an array', function () {
    const props = ['first', 'second'];
    const separator = faker.random.word();

    expect(pipe.transform(object, props, separator)).toBe(`${object.first}${separator}${object.second}`);
  });

});
