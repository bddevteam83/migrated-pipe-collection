import {Pipe, PipeTransform} from '@angular/core';
import {Icon} from './icon.model';
import * as _ from 'lodash';

// TODO cover with tests
@Pipe({
  name: 'toIcon'
})
export class ToIconPipe implements PipeTransform {

  transform(value: any, icons: Icon[]): string {
    if (value === null || undefined) {
      return null;
    }

    const res = icons.find(icon => _.isEqual(icon.case, value));

    return res ? res.icon : null;
  }

}
