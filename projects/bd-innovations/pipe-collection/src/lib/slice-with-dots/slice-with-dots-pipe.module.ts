import {NgModule} from '@angular/core';
import {SliceWithDotsPipe} from './slice-with-dots.pipe';

@NgModule({
  declarations: [SliceWithDotsPipe],
  exports: [SliceWithDotsPipe]
})
export class SliceWithDotsPipeModule {
}
