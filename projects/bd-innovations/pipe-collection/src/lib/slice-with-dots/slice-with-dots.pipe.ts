import {Pipe, PipeTransform} from '@angular/core';

/**
 * Checks whether specified string is bigger than some length and if so slices it and adds '...' in the end
 *
 * @param value The string to check its length
 * @param end The max length of the string when slicing isn't needed. By default is 30
 * @return string of specified value or sliced one with dots in the end; null if value is falsy
 */
@Pipe({
  name: 'sliceWithDots'
})
export class SliceWithDotsPipe implements PipeTransform {

  transform(value: string, end: number = 30): string {
    if (!value) {
      return value;
    }

    return value.length > end ? (value.substring(0, end) + '...') : value;
  }

}
