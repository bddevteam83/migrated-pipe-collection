import {SliceWithDotsPipe} from './slice-with-dots.pipe';
import * as faker from 'faker';

describe('SliceWithDotsPipe', () => {

  const value: string = faker.lorem.paragraph(10);
  const pipe = new SliceWithDotsPipe();

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should pass throw \'value\' parameter, when it is falsy', function () {
    expect(pipe.transform(null)).toBeNull();
    expect(pipe.transform(undefined)).toBeUndefined();
    expect(pipe.transform('')).toBe('');
  });

  it('should return sliced value when length of \'value\' parameter is longer than \'defaultEnd\' property', function () {
    const substringValue = `${value.substring(0, 30)}...`;

    expect(pipe.transform(value)).toBe(substringValue);
  });

  it('should return substring value when length of \'value\' parameter is longer than \'end\' parameter', function () {
    const end: number = faker.random.number({max: value.length - 1});
    const substringValue = `${value.substring(0, end)}...`;

    expect(pipe.transform(value, end)).toBe(substringValue);
  });

  it('should return given value when length of \'value\' parameter is smaller or equal to \'defaultEnd\' property', function () {
    const smallValue: string = value.substring(0, faker.random.number(30));
    expect(pipe.transform(smallValue)).toBe(smallValue);

    const equalValue: string = value.substring(0, 30);
    expect(pipe.transform(equalValue)).toBe(equalValue);
  });

  it('should return given value when length of \'value\' parameter is smaller or equal to \'end\' parameter', function () {
    const end: number = faker.random.number({min: value.length});

    const smallValue: string = value.substring(0, end - faker.random.number(end - 1));
    expect(pipe.transform(smallValue, end)).toBe(smallValue);

    const equalValue: string = value.substring(0, end);
    expect(pipe.transform(equalValue, end)).toBe(equalValue);
  });

});
