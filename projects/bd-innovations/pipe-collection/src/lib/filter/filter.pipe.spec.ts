import {FilterPipe} from './filter.pipe';

interface User {
  id: number;
  firstName: string;
  lastName: string;
  work: {
    title: string;
    company: string;
  };
}

describe('FilterPipe', () => {
  const pipe = new FilterPipe();
  const users: User[] = [
    {
      id: 1,
      firstName: 'John',
      lastName: 'Doe',
      work: {
        title: 'Software Engineer',
        company: 'Foo Tech',
      },
    },
    {
      id: 2,
      firstName: 'Jane',
      lastName: 'West',
      work: {
        title: 'Designer',
        company: 'AAA Solutions',
      },
    },
    {
      id: 3,
      firstName: 'Bruce',
      lastName: 'John',
      work: {
        title: 'Software Engineer',
        company: 'Bar Tech',
      },
    },
    {
      id: 4,
      firstName: 'William',
      lastName: 'Cent',
      work: {
        title: 'Designer',
        company: 'Foo Tech',
      },
    },
    {
      id: 5,
      firstName: 'George',
      lastName: 'Foreman',
      work: null,
    },
  ];

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return empty array in case of falsy array', () => {
    expect(pipe.transform(null, () => true)).toEqual([]);
    expect(pipe.transform(undefined, () => true)).toEqual([]);
  });

  it('should filters correctly', () => {
    const usersWithIdGreaterThanTwo = (user: User) => user.id < 2;
    expect(pipe.transform(users, usersWithIdGreaterThanTwo)).toEqual([
      {
        id: 1,
        firstName: 'John',
        lastName: 'Doe',
        work: {
          title: 'Software Engineer',
          company: 'Foo Tech',
        }
      }
    ]);

    const usersWithoutWork = (user: User) => !user.work;
    expect(pipe.transform(users, usersWithoutWork)).toEqual([
      {
        id: 5,
        firstName: 'George',
        lastName: 'Foreman',
        work: null,
      }
    ]);
  });
});
