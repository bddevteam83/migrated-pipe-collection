import {Pipe, PipeTransform} from '@angular/core';

import {filter as _filter} from 'lodash';

@Pipe({name: 'filter'})
export class FilterPipe implements PipeTransform {
  transform<T extends Record<PropertyKey, unknown>, K extends keyof T>(
    collection: T,
    predicate: (value: T[K], key: PropertyKey, collection: T) => boolean
  ): T[K][];
  transform<T>(
    collection: ArrayLike<T>,
    predicate: (value: T, index: number, collection: ArrayLike<T>) => boolean
  ): T[];
  transform(
    collection: string,
    predicate: (item: string, index: number, collection: string) => boolean
  ): string[] {
    if (!collection || !predicate) {
      return [];
    }

    return _filter(collection, predicate);
  }
}
