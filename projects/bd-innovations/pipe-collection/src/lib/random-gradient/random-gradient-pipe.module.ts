import {NgModule} from '@angular/core';
import {RandomGradientPipe} from './random-gradient.pipe';

@NgModule({
  declarations: [RandomGradientPipe],
  exports: [RandomGradientPipe]
})
export class RandomGradientPipeModule {
}
