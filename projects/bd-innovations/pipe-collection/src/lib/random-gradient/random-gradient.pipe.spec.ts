import {RandomGradientPipe} from './random-gradient.pipe';

import * as faker from 'faker';

describe('RandomGradientPipe', () => {
  const pipe = new RandomGradientPipe();

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should have not empty \'colorsArray\' as array of strings property', function () {
    expect('colorsArray' in pipe).toBeTruthy();
    expect(pipe.colorsArray instanceof Array).toBeTruthy();
    expect(pipe.colorsArray.length).toBeGreaterThan(0);

    let stringCheck = true;
    pipe.colorsArray.forEach(color => {
      if (typeof color !== 'string') {
        stringCheck = false;
      }
    });
    expect(stringCheck).toBeTruthy();
  });

  it('should return one of \'colorsArray\' elements regardless of whether the \'value\' parameter is specified', function () {
    expect(pipe.colorsArray).toContain(pipe.transform());
    expect(pipe.colorsArray).toContain(pipe.transform(faker.random.word()));
  });
});
