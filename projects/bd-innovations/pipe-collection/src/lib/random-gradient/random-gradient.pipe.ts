import {Pipe, PipeTransform} from '@angular/core';

/**
 * Converts specified value into one of class names to add random background color to HTML element
 *
 * @return string name of randomized class name
 */
@Pipe({
  name: 'randomGradient'
})
export class RandomGradientPipe implements PipeTransform {
  /**
   * An array of class names to randomize background from.
   * They should be manually implemented to project styles in order to work correctly.
   */
  readonly colorsArray: string[] = [
    'gradient-strawberry',
    'gradient-orange',
    'gradient-dawn',
    'gradient-shdow-night',
    'gradient-man-of-steel',
    'gradient-flickr',
    'gradient-ibiza-sunset',
    'gradient-politics'
  ];

  transform(value?): string {
    const randomNumber = Math.floor(Math.random() * this.colorsArray.length);

    return this.colorsArray[randomNumber];
  }

}
