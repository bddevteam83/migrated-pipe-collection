import {NgModule} from '@angular/core';
import {PrettyPrintPipe} from './pretty-print.pipe';

@NgModule({
  declarations: [PrettyPrintPipe],
  exports: [PrettyPrintPipe]
})
export class PrettyPrintPipeModule {
}
