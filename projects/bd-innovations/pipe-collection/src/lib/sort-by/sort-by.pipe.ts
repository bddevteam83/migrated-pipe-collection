import {Pipe, PipeTransform} from '@angular/core';
import * as _ from 'lodash';

// TODO cover with tests
@Pipe({
  name: 'sortBy'
})
export class SortByPipe implements PipeTransform {

  transform(collection: any, iterates: any): any {
    if (!collection) {
      return null;
    }

    return _.sortBy(collection, iterates);
  }

}
