import {OrderedKeyValuePipe} from './ordered-key-value.pipe';
import * as faker from 'faker';

describe('OrderedKeyValuePipe', () => {

  const pipe = new OrderedKeyValuePipe();

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return an empty array when \'object\' parameter is falsy', function () {
    expect(pipe.transform(null)).toEqual([]);
    expect(pipe.transform(undefined)).toEqual([]);
    expect(pipe.transform('')).toEqual([]);
  });

  it('should return array of keys and values of specified \'object\' in the same order the object was created with', function () {
    const object = {
      first: faker.random.word(),
      second: faker.random.word(),
      third: faker.random.word(),
      nested: {
        fourth: faker.random.word()
      }
    };
    const array = [
      {key: 'first', value: object.first},
      {key: 'second', value: object.second},
      {key: 'third', value: object.third},
      {key: 'nested', value: object.nested}
    ];

    expect(pipe.transform(object)).toEqual(array);
  });

});
