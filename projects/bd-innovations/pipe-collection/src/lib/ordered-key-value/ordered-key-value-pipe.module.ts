import {NgModule} from '@angular/core';
import {OrderedKeyValuePipe} from './ordered-key-value.pipe';

@NgModule({
  declarations: [OrderedKeyValuePipe],
  exports: [OrderedKeyValuePipe]
})
export class OrderedKeyValuePipeModule {
}
