import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'convertIdToParam'
})
export class ConvertIdToParamPipe implements PipeTransform {

  transform(value: string, args: Array<any>, param: string): string {
    if (args !== undefined) {
      let result = '';
      args.map(e => {
        result = e.id === value ? e[param] : value;
      });
      return result;
    } else {
      return value;
    }
  }

}
