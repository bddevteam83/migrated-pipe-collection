import {NgModule} from '@angular/core';
import {ConvertIdToParamPipe} from './convert-id-to-param.pipe';

@NgModule({
  declarations: [ConvertIdToParamPipe],
  exports: [ConvertIdToParamPipe]
})
export class ConvertIdToParamPipeModule {
}
