import {NullOrUndefinedHandlerPipe} from './null-or-undefined-handler.pipe';
import * as faker from 'faker';

describe('NullOrUndefinedHandlerPipe', () => {

  const pipe = new NullOrUndefinedHandlerPipe();
  const value = faker.random.word();

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should pass throw \'value\' parameter, when it isn\'t falsy', function () {
    expect(pipe.transform(value)).toBe(value);
  });

  it('should return \'defaultResult\', when given value is falsy and \'result\' wasn\'t passed', function () {
    expect(pipe.transform(null)).toBe('');
    expect(pipe.transform(undefined)).toBe('');
    expect(pipe.transform('')).toBe('');
  });

  it('should return \'result\', when given value is falsy and \'result\' was passed', function () {
    const result = faker.random.word();

    expect(pipe.transform(null, result)).toBe(result);
    expect(pipe.transform(undefined, result)).toBe(result);
    expect(pipe.transform('', result)).toBe(result);
  });

});
