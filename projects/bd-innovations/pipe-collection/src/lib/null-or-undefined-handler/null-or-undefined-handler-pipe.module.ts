import {NgModule} from '@angular/core';
import {NullOrUndefinedHandlerPipe} from './null-or-undefined-handler.pipe';

@NgModule({
  declarations: [NullOrUndefinedHandlerPipe],
  exports: [NullOrUndefinedHandlerPipe]
})
export class NullOrUndefinedHandlerPipeModule {
}
