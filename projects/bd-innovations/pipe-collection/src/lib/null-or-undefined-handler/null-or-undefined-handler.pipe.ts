import {Pipe, PipeTransform} from '@angular/core';

/**
 * Check whether variable is falsy and if so replaces it with specified or default placeholder
 *
 * @param value The value to check
 * @param placeholder The string to use if value is valid
 * @return Returns specified value or placeholder according to whether value is falsy
 */
@Pipe({
  name: 'nullOrUndefinedHandler'
})
export class NullOrUndefinedHandlerPipe implements PipeTransform {

  transform(value: any, placeholder: string = ''): any {

    return value ? value : placeholder;
  }

}
